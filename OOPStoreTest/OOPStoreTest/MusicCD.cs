﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace OOPStoreTest
{
    enum Music_type
    {
        Rock,
        Metal,
        Rap,
    };
    class MusicCD : Product
    {
        public string singer { get; }
        // public Music_type Mtype;



        public string conString;
        public MusicCD(int _id)
        {
            DatabaseConnection dtConnection = new DatabaseConnection();
            conString = dtConnection.conString;
            SqlConnection connection = new SqlConnection(conString);
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "SELECT * FROM [BookStore].[dbo].[Table_11],[BookStore].[dbo].[MusicCD] WHERE MusicCD.id=Table_11.id ";
            DataTable data = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(command);
            adapt.Fill(data);

            id = data.Rows[_id][0].ToString();
            name = data.Rows[_id][1].ToString();
            price = Convert.ToDouble(data.Rows[_id][2]);
            singer = data.Rows[_id][4].ToString();
            // Mtype = _type;

            connection.Close();



        }

        public override string PrintProperties()
        {
            return  id + "," + name + "," + price + "," + singer ;
        }
    }
}