﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace OOPStoreTest
{
    class Book :Product
    {
        public string ISBN { get; }
        public string author { get; }
        public string publisher { get; }
        public int Page { get; }

        public string conString;
        public Book(int _id)
        {
            DatabaseConnection dtConnection = new DatabaseConnection();
            conString=dtConnection.conString;
            SqlConnection connection = new SqlConnection(conString);
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "SELECT * FROM [BookStore].[dbo].[Table_11],[BookStore].[dbo].[Book] WHERE Book.id=Table_11.id";

            DataTable data = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(command);
            adapt.Fill(data);

            id = data.Rows[_id][0].ToString();
            name = data.Rows[_id][1].ToString();
            price = Convert.ToDouble(data.Rows[_id][2]);
            ISBN = data.Rows[_id][4].ToString();
            author = data.Rows[_id][5].ToString();
            publisher = data.Rows[_id][6].ToString();
            Page = Convert.ToInt32(data.Rows[_id][7]);
            connection.Close();
        }
       

       

        public override string PrintProperties()
        {
            return id + "," + name + "," + price + "," + ISBN + "," + author + "," + publisher + "," + Page;
        }

    }
}
