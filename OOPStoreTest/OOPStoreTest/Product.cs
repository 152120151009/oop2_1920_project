﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPStoreTest
{
    abstract class Product
    {
        public string name;
        public string id;
        public double price;

        public abstract string PrintProperties();
      

    }
}
