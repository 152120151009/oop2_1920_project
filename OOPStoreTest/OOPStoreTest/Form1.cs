﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace OOPStoreTest
{
    public partial class Form1 : Form
    {
        public Form1(string usName)
        {
            DatabaseConnection dtConnection = new DatabaseConnection();
            conString = dtConnection.conString;
            InitializeComponent();
            SqlConnection connection = new SqlConnection(conString);
            connection.Open();
            SqlDataAdapter sda = new SqlDataAdapter("Select * From LoginForms where Username='" + usName + "'", connection);
            DataTable data = new DataTable();
            sda.Fill(data);
            Customer cs0 = new Customer(usName);
            string[] cstmrInf = cs0.printCustomerDetails().ToString().Split(',');
            string[] cstmrtbs = { "customerID", "Name", "Adress", "Email", "Username" };
            label26.Text = cs0.Username;
            label18.Text = cs0.customerID.ToString();
            label7.Text = "";
            for (int i = 0; i < cstmrInf.Length; i++)
            {
                label7.Text += cstmrtbs[i] + ": " + cstmrInf[i] + Environment.NewLine;
            }


        }
        public string conString;

        public int aaa;
        Book bk0 = new Book(0);
        Book bk1 = new Book(1);
        Book bk2 = new Book(2);
        Book bk3 = new Book(3);
        Book bk4 = new Book(4);
        int items = 0;
        int[] mProds = new int[150];


        Magazine mg0 = new Magazine(0);
        Magazine mg1 = new Magazine(1);
        Magazine mg2 = new Magazine(2);
        Magazine mg3 = new Magazine(3);
        Magazine mg4 = new Magazine(4);

        MusicCD mC0 = new MusicCD(0);
        MusicCD mC1 = new MusicCD(1);
        MusicCD mC2 = new MusicCD(2);
        MusicCD mC3 = new MusicCD(3);
        MusicCD mC4 = new MusicCD(4);
        ItemsToPurchase purchases = new ItemsToPurchase();
        ShoppingCart sp = ShoppingCart.Instance;
        


        private void Form1_Load(object sender, EventArgs e)
        {
            
            int i = 0;
            string[] wordsBk0 = bk0.PrintProperties().Split(',');
            string[] wordsBk1 = bk1.PrintProperties().Split(',');
            string[] wordsBk2 = bk2.PrintProperties().Split(',');
            string[] wordsBk3 = bk3.PrintProperties().Split(',');
            string[] wordsBk4 = bk4.PrintProperties().Split(',');
            string[] tabsBooks = { "ID:", "Name:", "Price: ₺", "ISBN:", "Author:", "Publisher:", "Page:" };
            for (i = 0; i < 6; i++)
            {
                if (i < 2)
                {
                    label2.Text += tabsBooks[i] + " " + wordsBk0[i] + Environment.NewLine;
                }
                else if (i == 2)
                {
                    label2.Text += tabsBooks[2] + " " + wordsBk0[2] + "," + wordsBk0[3]+Environment.NewLine;
                }
                else
                {
                    label2.Text += tabsBooks[i] + " " + wordsBk0[i+1] + Environment.NewLine;
                }
                 }
            for (i = 0; i < 6; i++)
            {
                if (i < 2)
                {
                    label3.Text += tabsBooks[i] + " " + wordsBk1[i] + Environment.NewLine;
                }
                else if (i == 2)
                {
                    label3.Text += tabsBooks[2] + " " + wordsBk1[2] + "," + wordsBk1[3] + Environment.NewLine;
                }
                else
                {
                    label3.Text += tabsBooks[i] + " " + wordsBk1[i + 1] + Environment.NewLine;
                }
            }
            for (i = 0; i < 6; i++)
            {
                if (i < 2)
                {
                    label4.Text += tabsBooks[i] + " " + wordsBk2[i] + Environment.NewLine;
                }
                else if (i == 2)
                {
                    label4.Text += tabsBooks[2] + " " + wordsBk2[2] + "," + wordsBk2[3] + Environment.NewLine;
                }
                else
                {
                    label4.Text += tabsBooks[i] + " " + wordsBk2[i + 1] + Environment.NewLine;
                }
            }
            for (i = 0; i < 6; i++)
            {
                if (i < 2)
                {
                    label5.Text += tabsBooks[i] + " " + wordsBk3[i] + Environment.NewLine;
                }
                else if (i == 2)
                {
                    label5.Text += tabsBooks[2] + " " + wordsBk3[2] + "," + wordsBk3[3] + Environment.NewLine;
                }
                else
                {
                    label5.Text += tabsBooks[i] + " " + wordsBk3[i + 1] + Environment.NewLine;
                }
            }
            for (i = 0; i <6; i++)
            {
                if (i < 2)
                {
                    label6.Text += tabsBooks[i] + " " + wordsBk4[i] + Environment.NewLine;
                }
                else if (i == 2)
                {
                    label6.Text += tabsBooks[2] + " " + wordsBk4[2] + "," + wordsBk4[3] + Environment.NewLine;
                }
                else
                {
                    label6.Text += tabsBooks[i] + " " + wordsBk4[i + 1] + Environment.NewLine;
                }
            }

            string[] wordsMg0 = mg0.PrintProperties().Split(',');
            string[] wordsMg1 = mg1.PrintProperties().Split(',');
            string[] wordsMg2 = mg2.PrintProperties().Split(',');
            string[] wordsMg3 = mg3.PrintProperties().Split(',');
            string[] wordsMg4 = mg4.PrintProperties().Split(',');
            string[] tabsMg = { "ID:", "Name:", "Price: ₺", "Issue:"};
            for (i = 0; i < 4; i++)
            {
                if (i < 2)
                {
                    label13.Text += tabsMg[i] + " " + wordsMg0[i] + Environment.NewLine;
                }
                else if (i == 2)
                {
                    label13.Text += tabsMg[2] + " " + wordsMg0[2] + "," + wordsMg3[3] + Environment.NewLine;
                }
                else
                {
                    label13.Text += tabsMg[3] + " " + wordsMg0[4] + Environment.NewLine;
                }
 }
            for (i = 0; i < 4; i++)
            {
                if (i < 2)
                {
                    label12.Text += tabsMg[i] + " " + wordsMg1[i] + Environment.NewLine;
                }
                else if (i == 2)
                {
                    label12.Text += tabsMg[2] + " " + wordsMg1[2] + "," + wordsMg1[3] + Environment.NewLine;
                }
                else
                {
                    label12.Text += tabsMg[3] + " " + wordsMg1[4] + Environment.NewLine;
                }
            }
            for (i = 0; i < 4; i++)
            {
                if (i < 2)
                {
                    label11.Text += tabsMg[i] + " " + wordsMg2[i] + Environment.NewLine;
                }
                else if (i == 2)
                {
                    label11.Text += tabsMg[2] + " " + wordsMg2[2] + "," + wordsMg2[3] + Environment.NewLine;
                }
                else
                {
                    label11.Text += tabsMg[3] + " " + wordsMg2[4] + Environment.NewLine;
                }
            }
            for (i = 0; i <4; i++)
            {
                if (i < 2)
                {
                    label10.Text += tabsMg[i] + " " + wordsMg3[i] + Environment.NewLine;
                }
                else if (i == 2)
                {
                    label10.Text += tabsMg[2] + " " + wordsMg3[2] + "," + wordsMg3[3] + Environment.NewLine;
                }
                else
                {
                    label10.Text += tabsMg[3] + " " + wordsMg3[4] + Environment.NewLine;
                }
            }
            for (i = 0; i <4; i++)
            {
                if (i < 2)
                {
                    label9.Text += tabsMg[i] + " " + wordsMg4[i] + Environment.NewLine;
                }
                else if (i == 2)
                {
                    label9.Text += tabsMg[2] + " " + wordsMg4[2] + "," + wordsMg4[3] + Environment.NewLine;
                }
                else
                {
                    label9.Text += tabsMg[3] + " " + wordsMg4[4] + Environment.NewLine;
                }
            }

            string[] wordsMsc0 = mC0.PrintProperties().Split(',');
            string[] wordsMsc1 = mC1.PrintProperties().Split(',');
            string[] wordsMsc2 = mC2.PrintProperties().Split(',');
            string[] wordsMsc3 = mC3.PrintProperties().Split(',');
            string[] wordsMsc4 = mC4.PrintProperties().Split(',');
            string[] tabsMsc = { "ID:", "Name:", "Price: ₺", "Singer:", "Type:" };
            for (i = 0; i < 4; i++)
            {
                if (i < 2)
                {
                    label17.Text += tabsMsc[i] + " " + wordsMsc0[i] + Environment.NewLine;
                }
                else if (i == 2)
                {
                    label17.Text += tabsMsc[2] + " " + wordsMsc0[2] + "," + wordsMg3[3] + Environment.NewLine;
                }
                else
                {
                    label17.Text += tabsMsc[3] + " " + wordsMsc0[4] + Environment.NewLine;
                }
            }
            for (i = 0; i < 4; i++)
            {
                if (i < 2)
                {
                    label16.Text += tabsMsc[i] + " " + wordsMsc1[i] + Environment.NewLine;
                }
                else if (i == 2)
                {
                    label16.Text += tabsMsc[2] + " " + wordsMsc1[2] + "," + wordsMg1[3] + Environment.NewLine;
                }
                else
                {
                    label16.Text += tabsMsc[3] + " " + wordsMsc1[4] + Environment.NewLine;
                }
            }
            for (i = 0; i < 4; i++)
            {
                if (i < 2)
                {
                    label15.Text += tabsMsc[i] + " " + wordsMsc2[i] + Environment.NewLine;
                }
                else if (i == 2)
                {
                    label15.Text += tabsMsc[2] + " " + wordsMsc2[2] + "," + wordsMg2[3] + Environment.NewLine;
                }
                else
                {
                    label15.Text += tabsMsc[3] + " " + wordsMsc2[4] + Environment.NewLine;
                }
            }
            for (i = 0; i < 4; i++)
            {
                if (i < 2)
                {
                    label14.Text += tabsMsc[i] + " " + wordsMsc3[i] + Environment.NewLine;
                }
                else if (i == 2)
                {
                    label14.Text += tabsMsc[2] + " " + wordsMsc3[2] + "," + wordsMg3[3] + Environment.NewLine;
                }
                else
                {
                    label14.Text += tabsMsc[3] + " " + wordsMsc3[4] + Environment.NewLine;
                }
            }
            for (i = 0; i < 4; i++)
            {
                if (i < 2)
                {
                    label8.Text += tabsMsc[i] + " " + wordsMsc4[i] + Environment.NewLine;
                }
                else if (i == 2)
                {
                    label8.Text += tabsMsc[2] + " " + wordsMsc4[2] + "," + wordsMg4[3] + Environment.NewLine;
                }
                else
                {
                    label8.Text += tabsMsc[3] + " " + wordsMsc4[4] + Environment.NewLine;
                }
            }

        }



        private void button2_Click(object sender, EventArgs e)
        {
            string payType = "Cash";
            sp.customerID = Convert.ToInt32(label18.Text);
            DatabaseConnection dtConnection = new DatabaseConnection();
            conString = dtConnection.conString;
            SqlConnection connection = new SqlConnection(conString);
            connection.Open();

            string lists = "";
            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                lists += checkedListBox1.Items[i];
                if (i != checkedListBox1.Items.Count - 1)
                {
                    lists += ",";
                }
            }
            String query;

            query = "INSERT INTO Orders(customerID,item,paymentAmount,paymentType) VALUES( '" + sp.customerID + "' , '" + lists + "'" +
                     ",'" + Convert.ToInt32(sp.paymentAmount) + "','" + payType + "')";
            SqlCommand cmd = new SqlCommand(query, connection);
            cmd.ExecuteNonQuery();


            DataLog(sender as Button);
            sp.setPaymentType(new Cash());
            
            //label1.Text = sp.PlaceOrder();
            if (radioButton1.Checked == true)
            {
                MessageBox.Show(sp.PlaceOrder() + Environment.NewLine + sp.sendInvoicebyEmail());
            }
            if (radioButton2.Checked == true)
            {
                MessageBox.Show(sp.PlaceOrder() + Environment.NewLine + sp.sendInvoicebySMS());
            }
            if (radioButton2.Checked == false && radioButton1.Checked == false)
            {
                MessageBox.Show("Please Select Invoice Method.!");
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
         
            string payType = "Credit Card";
            sp.customerID = Convert.ToInt32(label18.Text);
            DatabaseConnection dtConnection = new DatabaseConnection();
            conString = dtConnection.conString;
            SqlConnection connection = new SqlConnection(conString);
            connection.Open();

            string lists="";
            for(int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                lists += checkedListBox1.Items[i];
            }
            String query;
          
               query = "INSERT INTO Orders(customerID,item,paymentAmount,paymentType) VALUES( '" + sp.customerID + "' , '" + lists + "'" +
                        ",'" + Convert.ToInt32(sp.paymentAmount) + "','" + payType + "')";
                SqlCommand cmd = new SqlCommand(query, connection);
                cmd.ExecuteNonQuery();
            
            
            
            DataLog(sender as Button);
            sp.setPaymentType(new CreditCart());
            //label1.Text = sp.PlaceOrder();

            if (radioButton1.Checked == true)
            {
                MessageBox.Show(sp.PlaceOrder() + Environment.NewLine + sp.sendInvoicebyEmail());
            }
            if (radioButton2.Checked == true)
            {
                MessageBox.Show(sp.PlaceOrder() + Environment.NewLine + sp.sendInvoicebySMS());
            }
            if (radioButton2.Checked == false && radioButton1.Checked == false)
            {
                MessageBox.Show("Please Select Invoice Method.!");
            }



        }

        public void DataLog(Button btn)
        {
            DatabaseConnection dtConnection = new DatabaseConnection();
            conString = dtConnection.conString;
            SqlConnection connection = new SqlConnection(conString);
            connection.Open();
            String query = "INSERT INTO Lgos(Username,ButtonName,Date,Time) VALUES( '" + label26.Text + "' , '" +btn.Text.ToString() + "'" +
                    ",'" + DateTime.Now.ToLongDateString() + "','" + DateTime.Now.ToLongTimeString() + "')";
            SqlCommand cmd = new SqlCommand(query, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        

        private void button4_Click(object sender, EventArgs e)
        {
            DataLog(sender as Button);
            int str = Convert.ToInt32((sender as Button).Tag.ToString());
            sp.addProduct(str);   
            label25.Text = "";
            mProds[items] = Convert.ToInt32((sender as Button).Tag.ToString());
            items++;
            checkedListBox1.Items.Add(sp.printProducts(mProds[items-1]));
          
            double totalpay = sp.paymentAmount;
            label19.Text =totalpay.ToString();
        

        }

        private void button1_Click(object sender, EventArgs e)
        {

            DataLog(sender as Button);
            checkedListBox1.Items.Clear();
            label19.Text = "0";
            items = 0;
            sp.cancelOrder();
        


        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            DataLog(sender as Button);

            for (int i = 0; i < checkedListBox1.CheckedIndices.Count; i++)
            {
                double totalpay=0;
                totalpay =sp.removeProduct(mProds[checkedListBox1.CheckedIndices[i]]);
                label19.Text = totalpay.ToString();
                items--;

            }
   
            foreach (var item in checkedListBox1.CheckedItems.OfType<string>().ToList())
            {
                checkedListBox1.Items.Remove(item);
            }
         
        }

        private void groupBox12_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            
            Form2 report = new Form2();
            report.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Form3 report = new Form3();
            report.Show();
        }
    }
}
