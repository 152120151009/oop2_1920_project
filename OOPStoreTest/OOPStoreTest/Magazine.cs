﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;


namespace OOPStoreTest
{
    enum Mag_type
    {
        Actual,
        News,
        Sport,
        Computer,
        Technology
    };
    class Magazine : Product
    {
   

       // public Mag_type mType;
        public string issue { get; }

        public string conString;
        public Magazine(int _id)
        {
            DatabaseConnection dtConnection = new DatabaseConnection();
            conString = dtConnection.conString;
            SqlConnection connection = new SqlConnection(conString);
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "SELECT * FROM [BookStore].[dbo].[Table_11],[BookStore].[dbo].[Magazine] WHERE Magazine.id=Table_11.id ";
            DataTable data = new DataTable();
            SqlDataAdapter adapt = new SqlDataAdapter(command);
            adapt.Fill(data);

            id = data.Rows[_id][0].ToString();
            name = data.Rows[_id][1].ToString();
            price = Convert.ToDouble(data.Rows[_id][2]);
            issue = data.Rows[_id][4].ToString();
            // mType = _type;

            connection.Close();




        }
    


        public override string PrintProperties()
        {
            return  id + "," + name + "," + price + "," + issue ;
        }
    }

}