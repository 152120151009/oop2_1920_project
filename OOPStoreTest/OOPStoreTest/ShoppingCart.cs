﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace OOPStoreTest
{
    class ShoppingCart 
    {
        public int customerID;
        public PaymentType _paymentType;
        public static ShoppingCart instance = null;
        public List<string> myİtems = new List<string>();
        ArrayList itemsToPuchase = new ArrayList();
        public double paymentAmount { get; set; }
        


        private ShoppingCart()
        {
            this.customerID = 1;
        }

        public static ShoppingCart Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ShoppingCart();
                }
                return instance;
            }
        }

        public string printProducts(int _ad)
        {
            string s="";


            if (_ad <= 4)
            {
                Book bk = new Book(_ad);
                s = bk.name.ToString() ;

            }
            if (4 < _ad && _ad <= 9)
            {

                Magazine mg = new Magazine(_ad - 5);
                s = mg.name.ToString();

            }
            if (9 < _ad && _ad <= 14)
            {
                MusicCD mg = new MusicCD(_ad - 10);
                s = mg.name.ToString() ;

            }



            return s;




        }

        public double addProduct(int _ad)
        {
          
            
           
            if (_ad <= 4)
            {
                
                Book bk = new Book(_ad);
                paymentAmount += bk.price;
                
            }
            if (4 < _ad && _ad <= 9)
            {
                
                Magazine mg = new Magazine(_ad-5);
                paymentAmount += mg.price;
            }
            if (9 < _ad && _ad <= 14)
            {
                MusicCD mg = new MusicCD(_ad-10);
                paymentAmount += mg.price;
            

            }
            itemsToPuchase.Add(_ad);
            return paymentAmount;



        }

        public double removeProduct(int _ad)
        {
            itemsToPuchase.Remove(_ad);
            if (_ad <= 4)
            {
                Book bk = new Book(_ad);
                paymentAmount -= bk.price;
            }
            if (4 < _ad && _ad <= 9)
            {

                Magazine mg = new Magazine(_ad - 5);
                paymentAmount -= mg.price;
            }
            if (9 < _ad && _ad <= 14)
            {
                MusicCD mg = new MusicCD(_ad - 10);
                paymentAmount -= mg.price;

            }
          

            printProducts(_ad);
            return paymentAmount;
        }

        public void cancelOrder()
        {
            paymentAmount = 0;
            itemsToPuchase.Clear();
            

        }
        public string sendInvoicebySMS()
        {
            return "Invoice Sended By SMS";

        }
        public string sendInvoicebyEmail()
        {
            return "Invoice Sended By EMAİL";
        }





        public void setPaymentType(PaymentType __paymentType)
        {
            _paymentType = __paymentType;
         
        }

        public string PlaceOrder()
        {
            return this._paymentType.PlaceOrder();

        }

    
    }
    public interface PaymentType
    {
        string PlaceOrder();
    }


    public class CreditCart : PaymentType
    {
        public string PlaceOrder()
        {
            return "Payment Made By Credit Cart";
        }

    }

    public class Cash : PaymentType
    {
        public string PlaceOrder()
        {
            return "Payment Made By Cash";
        }
    }
}
